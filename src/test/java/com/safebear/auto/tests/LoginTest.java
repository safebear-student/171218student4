package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;


    public class LoginTest extends BaseTest {

        @Test
        public void LoginTest(){

            //Step 1 ACTION: Open our web application in the browser
            driver.get(Utils.getUrl());

            //Step 1 Expected: check we're on the Login Page
            Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());

            //Step 2 Action: Enter Username and Password
            loginPage.enterUsername("tester");
            loginPage.enterPassword("letmein");

            //Step 3 Action: Press the login button
            loginPage.clickLoginButton();

            //Step 3 Expected: check that we're now on the tools page
            Assert.assertEquals(toolspage.getPageTitle(), "Tools Page", "The login page didn't open, or the title page text has changed");

            //Step 4 Expected: check that Login successful message is displayed
            Assert.assertTrue(toolspage.getSuccessfulLogin().contains("Success"));
        }

        @Test
        public void InvalidLoginTest() {

            //Step 1 Action: open web application in browser
            driver.get(Utils.getUrl());

            //Step 1 Expected: check we're on the login page
            Assert.assertEquals(loginPage.getPageTitle(), "Login Page", "The Login page didn't open, or the title text has changed");

            //Step 2 Action: Enter username and invalid password
            loginPage.enterUsername("tester");
            loginPage.enterPassword("flyingbanana");

            //Step 3 Action: Click the login button
            loginPage.clickLoginButton();

            //Step 3 Expected: check that we're still on the login page
            Assert.assertEquals(loginPage.getPageTitle(), "Login Page" , "I've been logged in with an invalid password");

            //Step 4 Expected: Check failed login warning is displayed
            Assert.assertTrue(loginPage.getFailedLogin().contains("WARNING"));
        }

    }

