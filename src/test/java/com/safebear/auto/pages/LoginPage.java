package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {

    LoginPageLocators loginPageLocators = new LoginPageLocators();

    @NonNull
    WebDriver driver;
    String expectedPageTitle = "Login Page";

    public String getExpectedPageTitle(){
        return expectedPageTitle;
    }
    public String getPageTitle(){
        return driver.getTitle();
    }

    public void enterUsername (String username){
        driver.findElement(loginPageLocators.getUsernameFieldLocator()).sendKeys(username);
    }
    public void enterPassword (String password){
        driver.findElement(loginPageLocators.getPasswordFieldLocator()).sendKeys(password);
    }

    public void clickLoginButton() {
        driver.findElement(loginPageLocators.getLoginButtonLocator()).click();
    }
    public void login (String username, String password){
        enterUsername(username);
        enterPassword(password);
        clickLoginButton();
    }
    public String getFailedLogin(){
        return driver.findElement(loginPageLocators.getUnsuccessfulLoginMessage()).getText();
    }
}

