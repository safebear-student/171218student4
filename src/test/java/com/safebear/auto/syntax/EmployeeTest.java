package com.safebear.auto.syntax;

import com.google.common.annotations.VisibleForTesting;
import org.testng.annotations.Test;

public class EmployeeTest {

    @Test
    public void testEmployee() {

        //This is where we create our objects
        Employee hannah = new Employee();
        Employee bob = new Employee();
        SalesEmployee victoria = new SalesEmployee();

        //This is where we employ hannah and fire bob
        hannah.employ();
        bob.fire();

        //This is where we employ victoria and give her a ford
        victoria.employ();
        victoria.changeCar("Ford");

        //Let's print their state to screen
        System.out.println("Hannah employment state: " + hannah.employed);
        System.out.println("Bob employment state: " + bob.employed);
        System.out.println("Victoria employment state: " + victoria.employed);
        System.out.println("Victoria's car: " + victoria.car);
    }
    }

